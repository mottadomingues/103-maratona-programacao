package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class App {	
	public static void main(String[] args){
		String caminhoArquivo = "/home/a2/eclipse-workspace/exercicio_maratona/103-maratona-programacao/src/alunos.csv";
		List<Aluno> listaTodosAlunos = (new Arquivo(caminhoArquivo)).ler();
		
		Impressora.imprimir
		(
			ListaEquipeFactory.gerarListaEquipes
			(
				Sorteador.dividirAlunosPorQuantidadeMaxima(listaTodosAlunos, Equipe.QTD_MAX_ALUNOS)
			)
		);
	}

}
