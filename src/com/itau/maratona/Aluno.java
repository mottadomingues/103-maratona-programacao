package com.itau.maratona;

public class Aluno {
	public String nome;
	public String cpf;
	public String email;

	@Override
	public String toString()
	{
		return this.nome + " - CPF: " + this.cpf;
	}
}
