package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class Impressora
{
	public static String SEPARADOR_LINHA = "-------------------------------------------------------------";
	
	public static void imprimir(Object valor){
		System.out.println(valor);
	}
	
	public static void imprimir(List<Equipe> listaEquipe){
		for(Equipe iteradorEquipe : listaEquipe)
		{
			Impressora.imprimir(iteradorEquipe + ":");
			for(Aluno iteradorAluno : iteradorEquipe.listaAlunos)
			{
				Impressora.imprimir(iteradorAluno);
			}
			Impressora.imprimir(Impressora.SEPARADOR_LINHA);
		}
	}
}
