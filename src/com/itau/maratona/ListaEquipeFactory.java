package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class ListaEquipeFactory {

	public static List<Equipe> gerarListaEquipes(List<List<Aluno>> listaSegregadaAlunos)
	{
		List<Equipe> listaEquipe = new ArrayList<Equipe>();
		for(List<Aluno> iterador : listaSegregadaAlunos)
		{
			listaEquipe.add(new Equipe(iterador));
		}
		return listaEquipe;
	}	
}
