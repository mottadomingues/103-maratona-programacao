package com.itau.maratona;

import java.util.List;

public class Equipe {

	public static int CURRENT_ID = 0;
	public final static  int QTD_MAX_ALUNOS = 3;
	
	public int id;
	public List<Aluno> listaAlunos; 
	
	public void set_listaAlunos(List<Aluno> lstAlunos)
	{
		this.listaAlunos = lstAlunos;
	}
	
	public Equipe(List<Aluno> listaAlunos)
	{
		this.set_listaAlunos(listaAlunos);
		this.id = ++Equipe.CURRENT_ID;
	}
	
	@Override
	public String toString()
	{
		return "EQUIPE ID " + this.id;
	}
}
