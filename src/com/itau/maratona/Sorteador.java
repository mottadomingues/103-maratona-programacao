package com.itau.maratona;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Sorteador {
	public static List<List<Aluno>> dividirAlunosPorQuantidadeMaxima(List<Aluno> listaAlunos, int qtdMaxAlunos)
	{
		int posicaoSorteada = -1;
		
		List<Aluno> listaAlunosAgrupada = new ArrayList<Aluno>();
		List<List<Aluno>> listas = new ArrayList<List<Aluno>>();
		
		while(listaAlunos.size() > 0)
		{
			posicaoSorteada = (new Random()).nextInt(listaAlunos.size());
			listaAlunosAgrupada.add(listaAlunos.get(posicaoSorteada));
			listaAlunos.remove(posicaoSorteada);
			
			if(listaAlunosAgrupada.size() == qtdMaxAlunos || listaAlunos.size() == 0)
			{
				listas.add(listaAlunosAgrupada);
				if(listaAlunos.size() > 0)
					listaAlunosAgrupada = new ArrayList<Aluno>();
			}
		}
		
		return listas;
	}
}
